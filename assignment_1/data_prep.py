from __future__ import print_function

import matplotlib.pyplot as plt
import numpy as np
import os
import sys
import tarfile

from IPython.display import display, Image
from scipy import ndimage
from sklearn.linear_model import LogisticRegression
from six.moves.urllib.request import urlretrieve
from six.moves import cPickle as pickle

# Download file if it's not present and make sure it's the right size
def maybeDownload(filename, expected_bytes, force=False):
    if force or not os.path.exists(filename):
        filename, _ = urlretrieve(url + filename, filename) # returns filename, headers (response header of the server)
    statinfo = os.stat(filename)
    if statinfo.st_size == expected_bytes:
        print('Found and verified', filename)
    else:
        raise Exception('Failed to verify ' + filename + '.')
    return filename

# extract dataset from compressed tar file
def maybeExtract(filename, force=False):
    root = os.path.splitext(os.path.splitext(filename)[0])[0] # remove tar.gz
    if os.path.isdir(root) and not force:
        # can override by setting force=True
        print('%s already present - skipping %s extraction.' % (root, filename))
    else:
        print('Extracting data for %s. This may take a while. Please wait.' % root)
        tar = tarfile.open(filename)
        sys.stdout.flush() # write everything in the buffer to terminal
        tar.extractall()
        tar.close()
    data_folders = [ os.path.join(root, d) for d in sorted(os.listdir(root))
                     if os.path.isdir(os.path.join(root, d)) ]
    if len(data_folders) != num_classes:
        raise Exception('Expected %d folders, one per class. Found %d instead.' % (num_classes, len(data_folders)))
    print(data_folders)
    return data_folders

# Load the data for a single letter
def loadLetter(folder, min_num_images):
    image_files = os.listdir(folder)
    dataset = np.ndarray(shape=(len(image_files), image_size, image_size),
                         dtype=np.float32)
    image_index = 0
    print(folder)
    for image in os.listdir(folder):
        image_file = os.path.join(folder, image)
        try:
            image_data (ndimage.imread(image_file).astype(float) - pixel_depth / 2) / pixel_depth
            if image_data.shape != (image_size, image_size):
                raise Exception('Unexpected image shape: %s' % str(image_data.shape))
            dataset[image_index, :, :] = image_data
            image_index += 1
        except IOError as e:
            print('Could not read file - skipping.')

# store our data as .pickle... pickle our data. pickle it.
def maybePickle(data_folders, min_num_images_per_class, force=False):
    dataset_names = []
    for folder in data_folders:
        set_filename = folder + '.pickle'
        dataset_names.append(set_filename)
        if os.path.exists(set_filename) and not force:
            print('%s already present - skipping.' % set_filename)
        else:
            print('Pickling %s.' % set_filename)
            dataset = load_letter(folder, min_num_images_per_class)
            try:
                with open(set_filename, 'wb') as f: # 'wb' = write binary
                    pickle.dump(dataset, f, pickle.HIGHEST_PROTOCOL)
            except Exception as e:
                print('Unable to save data to', set_filename, ':', e)

def makeArrays(num_rows, img_size):
    if num_rows:
        dataset = np.ndarray((num_rows, img_size, img_size), dtype+np.float32)
        labels = np.ndarray(num_rows, dtype=np.int32)
    else:
        dataset, labels = None, None
    return dataset, labels

# merge and prune the training data and create a validation dataset
def mergeDatasets(pickle_files, train_size, valid_size=0):
    num_classes = len(pickle_files)
    valid_dataset, valid_labels = make_arrays(valid_size, image_size)
    train_dataset, train_labels = make_arrays(train_size, image_size)
    vsize_per_class = valid_size // num_classes
    tsize_per_class = train_size // num_classes

    start_v, start_t = 0, 0
    end_v, end_t = vsize_per_class, tsize_per_class
    end_l = vsize_per_class + tsize_per_class
    for label, pickle_file in enumerate(pickle_files):
        try:
            with open(pickle_file, 'rb') as f:
                letter_set = pickle.load(f)
                # shuffle letters to have random
                np.random.shuffle(letter_set)
                if valid_dataset is not None:
                    valid_letter = letter_set[:vsize_per_class, :, :]
                    valid_dataset[start_v:end_v, :, :] = valid_letter
                    valid_dataset[start_v:end_v] = label
                    start_v += vsize_per_class
                    end_v += vsize_per_class

                train_letter = letter_set[vsize_per_class, :, :]
                train_dataset[start_t:end_t, :, :] = train_letter

# randomize data to have well-shuffled labels for training + test distributions
def randomize(dataset, labels):
    permutation = np.random.permutation(labels.shape[0])
    shuffled_dataset = dataset[permutation, :, :]
    shuffled_labels = labels[permutation]
    return shuffled_dataset, shuffled_labels

url = 'http://yaroslavvb.com/upload/notMNIST/'

# This hangs for a long time (large file is genuinely large?) so just use
# the already downloaded version since we have no way of keeping track of
# the download implemented here
#train_filename = maybeDownload('notMNIST_large.tar.gz', 247336696)
#test_filename = maybeDownload('notMNIST_small.tar.gz', 8458043)
train_filename = 'notMNIST_large.tar.gz'
test_filename = 'notMNIST_small.tar.gz'

num_classes = 10
np.random.seed(133)

train_folders = maybeExtract(train_filename)
test_folders = maybeExtract(test_filename)

# Take a peek at some data to make sure it looks sensible
Image(os.getcwd() + '/notMNIST_small/A/MDEtMDEtMDAudHRM.png')

# Load data into more manageable format
# Load each class into a separate dataset, store them on disk, and curate them independently
image_size = 28 # pixel width and height
pixel_depth = 255.0  # number of levels per pixel

train_datasets = maybePickle(train_folders, 45000)
test_datasets = maybePickle(test_folders, 1800)

# verify data still looks good after pickling
file = open(train_datasets[0], 'rb')
train_A = pickle.load(file)
train_A.shape

plt.imshow(train_A[0,:,:])

# We expect data to be balanced across classes, so check that
print('Checking number of images per class...')
for letter in train_datasets:
    file = open(letter, 'rb')
    dataset = pickle.load(file)
    print(letter + 'size: ' + str(dataset.shape[0]))
for letter in test_datasets:
    file = open(letter, 'rb')
    dataset = pickle.load(file)
    print(letter + ' size: ' + str(dataset.shape[0]))

train_size = 200000
valid_size = 10000
test_size = 10000

valid_dataset, valid_labels, train_dataset, train_labels = merge_datasets(train_datasets, train_size, valid_size)

_, _, test_dataset, test_labels = merge_datasets(test_datasets, test_size)

print('Training: ', train_dataset.shape, train_labels.shape)
print('Validation: ', valid_dataset.shape, valid_labels.shape)
print('Testing: ', test_dataset.shape, test_labels.shape)

train_dataset, train_labels = randomize(train_dataset, train_labels)
test_dataset, test_labels = randomize(test_dataset, test_labels)
valid_dataset, valid_labels = randomize(valid_dataset, valid_labels)
